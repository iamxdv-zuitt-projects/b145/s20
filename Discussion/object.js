//Creating object using blueprints 

//we will create our own object constructor
function Pokemon(name, level) {
	//Properties
	this.name = name; 
	this.level = level; 
	this.attack = 2 * level; 
	this.health = 2 * level; 
	//Methods 
	this.showHealth = function() {
		console.log('HEALTH POINT: ' + this.health); 
	}; 
	this.tackle = function(targetPokemon) {
       console.log(this.name + ' has t+ackled ' + targetPokemon.name);
       //what if you want to access the target pokemon's health?
       console.log(targetPokemon.health - this.attack); 
	}; 
	this.faint = function() {
       console.log(this.name + ' fainted. '); 
	}; 
}

//Create instances of our objects 
let pikachu = new Pokemon('Pikachu', 16); 
let ratata = new Pokemon('Ratata', 8);
