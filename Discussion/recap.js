//RECAP

//2 methods to create an Object in JS

//1. Object Literal (Curly braces)
let person = {
    //key -> value 
    //Properties
    name: 'John',
    weight: '50kg',
    height: '168cm', 
    //Objects
    location: {
      city: 'Metro Manila', //cavite
      region: 'NCR'
    },
    //Array
    friends: ['Marie', 'Johnny', 'Melody'],
    //Methods/Functions
    walk: function() {
      console.log('Person is Walking'); 
    },
    talk: function() {
      console.log('Person is Talking'); 
    }
  }

  console.log(person); 

//2. Object Constuctor/Initializer 
     //2 ways to declare props in using the constructor. 

  let car = Object({
     color: 'white',
     model: 'honda'
  }); 

  //[SUBSECTION] 

  let player = Object(); 
  //check if talagang object ang maccreate using the constructor
  console.log(typeof player);
  console.log(player); 
  //syntax: object.propName = value; 

  player.height = '189cm'; 
  player.skills = ['Basketball', 'Volleyball', 'Tennis'];
  player.talent = 'singing'; 
  console.log(player); 

  //Access props of an object

  //1. dot notation(.)
  console.log(player.talent); //singing => DANCING
  //2. square brackets []
  console.log(player['skills']);   
  

  //[REASSIGN A VALUE OF A PROPERTY]
  //(.) dot notation 
  player.talent = 'Dancing'; 
  console.log(player.talent); 

  console.log(person.location); 
  
  person['location'].city = 'Cavite'; 
  console.log(person.location.city); 

  //[Is it possible to reassign properties of a constant?]

  const laptop = Object(); 
  laptop.color = 'Black'; 
  laptop.model = 'ASUS'; 
  //reassign value of a prop
  laptop.color = 'Pink'; 
  console.log(laptop); 

  //destroy/delete a property of an object
  //USE the delete keyword to remove a property.
  delete laptop.model;  
  console.log(laptop); 
